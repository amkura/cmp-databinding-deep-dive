import {
  AfterContentChecked,
  AfterContentInit, AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck, ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges, ViewChild,
  ContentChild
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements
  OnInit,
  OnChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked ,
  AfterViewInit,
  AfterViewChecked {
  // setting an alias, so to access property from the outside you have to target [srvElement]
  // @Input('srvElement') element: {
  //   type: string,
  //   name: string,
  //   content: string
  // };
  @Input() name: string;
  @ViewChild('heading', {static: true}) header: ElementRef;
  @ContentChild('contentParagraph', {static: true}) paragraph: ElementRef;

  constructor() {
    console.log('Constructor called');
  }

  // runs before ngOnInit and then after every input change
  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges called', changes);
  }

  ngOnInit(): void {
    console.log('ngOnInit called');
    console.log('Header: ', this.header.nativeElement.textContent);
    console.log('Paragraph: ', this.paragraph.nativeElement.textContent);
  }

  // runs whenever angular checks for any changes; use if you ever need to change something angular didn't pick up
  ngDoCheck() {
    console.log('ngDoCheck called');
  }

  // called only one whenever projected ng-content is created; gets access to ng-content
  ngAfterContentInit() {
    console.log('ngAfterContentInit called');
    console.log('Paragraph: ', this.paragraph.nativeElement.textContent);
  }

  ngAfterContentChecked() {
    console.log('ngAfterContentChecked called');
  }

  // gets access to template elements
  ngAfterViewInit() {
    console.log('ngAfterViewInit called');
    console.log(this.header.nativeElement.textContent);
  }

  ngAfterViewChecked() {
    console.log('ngAfterViewChecked called');
  }
}
